using _741.Data;
using _741.Mappings;
using _741.Other;
using _741.Service;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using _741.Interface;
using _741.Schedule;
using _741.ViewModels;
using _741.Helper;
using _741.Models;

namespace _741
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //從組態讀取登入逾時設定
            double loginExpireMinute = Configuration.GetValue<double>("LoginExpireMinute");
            //註冊 CookieAuthentication，Scheme必填
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(option =>
            {
                //或許要從組態檔讀取，斟酌決定
                option.LoginPath = new PathString("/Login/Login");//登入頁
                option.LogoutPath = new PathString("/Login/Logout");//登出Action
                option.AccessDeniedPath = new PathString("/Login/NoPermission");
                //用戶頁面停留太久，登入逾期，或Controller的Action裡用戶登入時，也可以設定↓
                option.ExpireTimeSpan = TimeSpan.FromMinutes(loginExpireMinute);//沒給預設14天
                //↓資安建議false，白箱弱掃軟體會要求cookie不能延展效期，這時設false變成絕對逾期時間
                //↓如果明明一直在使用系統卻容易被自動登出的話，再設為true(然後弱掃policy請客戶略過此項檢查) 
                option.SlidingExpiration = false;
            });

            //api post無法使用
            //services.AddControllersWithViews(options =>
            //{
            //    //↓和CSRF資安有關，這裡就加入全域驗證範圍Filter的話，待會Controller就不必再加上[AutoValidateAntiforgeryToken]屬性
            //    options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            //});

            services.AddControllersWithViews();
            string drinkconnectionStr = Configuration.GetConnectionString("DrinkconnStr");

            services.AddDbContext<DrinkContext>(optionBuilder => optionBuilder.UseMySql(
            drinkconnectionStr, ServerVersion.AutoDetect(drinkconnectionStr)));
            //sqlconnection
            services.AddScoped<IDbConnection, MySqlConnection>(options =>
             {
                 MySqlConnection conn = new MySqlConnection();
                 conn.ConnectionString = Configuration.GetConnectionString("AllitemconnStr");
                 return conn;
             });

            services.AddScoped<MemberService>();
            services.AddScoped<LoginService>();
            services.AddScoped<PermissionService>();
            services.AddScoped<ManagePermissionsService>();
            services.AddScoped<CommonService>();
            services.AddScoped<RemoteConnectionService>();
            services.AddScoped<PermissionRoleService>();
            services.AddScoped<SettingJsonFileService>();
            //------
            services.AddScoped<IAllitemService, AllitemService>();
            services.AddScoped<DateClass>();
            services.AddScoped<InputObj>();



            services.AddAutoMapper(config =>
            {
                config.AddProfile<ServiceMapping>();
            });

            //啟用Session
            services.AddDistributedMemoryCache(); //使用內存記憶體來記錄Session
            services.AddSession(); //在DI容器中加入Session服務

            //services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>() 報錯
            services.AddHttpContextAccessor();
            
            //修改上傳檔案大小限制
            //Microsoft.AspNetCore.Http.Features.FormOptions
            services.Configure<FormOptions>(options =>
            {
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = int.MaxValue;
                options.MemoryBufferThreshold = int.MaxValue;
            });

            //httpclientFactory設定
            services.AddHttpClient("base_Httpclient", config =>
            {
                //config.BaseAddress = new Uri();
            });

            //hangfire
            services.AddHangfire(config =>
            {
                config.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseDefaultTypeSerializer()
                .UseMemoryStorage();
            });
            services.AddHangfireServer();
            //------------------------------- services.AddSingleton<IInsertDBSchedule, InsertDBSchedule>();
            services.AddScoped<IInsertDBSchedule, InsertDBSchedule>();
            services.AddSingleton<HangfireDashboardAccessAuthFilter>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IBackgroundJobClient backgroundJobClient,
            IRecurringJobManager recurringJobManager, IServiceProvider serviceProvider,HangfireDashboardAccessAuthFilter hangfireDashboardAccessAuthFilter)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //Authentication(驗證,會員登入)
            app.UseAuthentication();
            //Authorization(授權,角色與權限)
            app.UseAuthorization();

            //Cookie
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Cookies.Append("測試_Key", "測試_Value", new CookieOptions()
            //    {
            //        HttpOnly = false
            //    }); ;
            //    await next.Invoke();
            //});
            //注意要放在UseEndpoints之前

            //啟用Session
            app.UseSession();
            //app.Use(async (context, next) =>
            //{
            //    //Response Cookies有多了一個key為.AspNetCore.Session的Cookie，而這個就是用來識別Session的唯一值。
            //    context.Session.SetString("測試Key", "測試Value");
            //    await next.Invoke();
            //});

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Login}/{id?}");
            });
            //預設遠端不能看
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { hangfireDashboardAccessAuthFilter }
            }) ;
            backgroundJobClient.Enqueue(() => Console.WriteLine("Hangfire Begin"));

            List<ScheduleSettingObj> scheduleSettingObjs = new List<ScheduleSettingObj>();
            Configuration.GetSection("ScheduleSettingObjs").Bind(scheduleSettingObjs);
            foreach (var item in scheduleSettingObjs)
            {
                if (item.IntervalType == "Week")
                {
                    recurringJobManager.AddOrUpdate(item.ScheduleName, () => serviceProvider.GetService<IInsertDBSchedule>()
                    .CommonWrok("WriteText"), Cron.Weekly((DayOfWeek)item.DayofWeek,item.Hours, item.Minutes));
                }
                if (item.IntervalType == "Day")
                {
                    recurringJobManager.AddOrUpdate(item.ScheduleName, () => serviceProvider.GetService<IInsertDBSchedule>()
                    .CommonWrok("InsertAllItems"), Cron.Daily(item.Hours, item.Minutes));
                }
            }                  
        }
    }
}
